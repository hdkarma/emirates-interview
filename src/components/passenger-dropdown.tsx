import { Button, Container, Grid, makeStyles, Popover, TextField, Typography } from '@material-ui/core';
import React from 'react';
import 'react-google-flight-datepicker/dist/main.css';
import * as _ from 'lodash'


const useStyles = makeStyles((theme) => ({
    search_btn: {
        backgroundColor: '#c60c30',
        color: '#fff',
        height: 55,
        width: '100%',
        textAlign: 'center'
    },
    full_width: {
        width: '100%'
    },
    drop_title:{
        fontWeight: 'bold',
        lineHeight: 'initial',
        textAlign: 'center'
    },
    drop_sub:{
        fontSize: 10,
        textAlign: 'center'
    }
}));


export default function HomePersonspicker() {

  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const [passengerCountLabel, setPassengerCountLabel] = React.useState('');

  const [ButtonStates, setButtonStates] = React.useState({
        adult: {
            increment: false,
            decrement: true
        },
        children: {
            increment: false,
            decrement: true
        },
        infant: {
            increment: false,
            decrement: true
        }
    });


  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;


  const totalLimit = 9



const [dropDownData, setDropDownData] = React.useState([
    {
        code: "adult",
        name: "Adults",
        content: "Ages 12+",
        count: 0,
        min: 1
    },
    {
        code: "children",
        name: "Childrens",
        content: "Ages 2-11",
        count: 0,
        min: 0
    },
    {
        code: "infant",
        name: "Infant",
        content: "Age under 2",
        count: 0,
        min: 0
    }
]);


    const stateSetter = (_dropDownData:any, i:number, element:any, button:string) => {
        let buttonState = _.cloneDeep(ButtonStates)
        switch(_dropDownData[i].code){
            case 'adult': {
                if(_dropDownData[i].count === 1)
                    _.set(buttonState, `${element.code}.${button}`, true)
                else if(_.get(buttonState, `${element.code}.${button}`) != false)
                    _.set(buttonState, `${element.code}.${button}`, false)
                break;
            }
            case 'children':
                case 'infant': {
                if(_dropDownData[i].count === 0)
                    _.set(buttonState, `${element.code}.${button}`, true)
                else if(_.get(buttonState, `${element.code}.${button}`) != false)
                    _.set(buttonState, `${element.code}.${button}`, false)
                break;
            }
            default: break;
        }
        setButtonStates(buttonState)
    }

    const runIncrementDecrementChecker = (_dropDownData:any, i:number, element:any) => {
        stateSetter(_dropDownData, i, element, 'increment')
        stateSetter(_dropDownData, i, element, 'decrement')
    }

  return (
    <Grid className={classes.full_width}>
        <TextField id="outlined-basic" onClick={handleClick} label={passengerCountLabel} variant="outlined" className={classes.full_width} />

       <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
            }}
            transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
            }}>
                {dropDownData.map((element, i) => {
                    return ( 
                        <Grid container direction="column" key={"item"+i}>
                            <Grid container
                                    direction="row"
                                    justify="space-between"
                                    alignItems="center"
                                    className={classes.full_width}>
                                        <Button onClick={o => {
                                            let _dropDownData = _.cloneDeep(dropDownData)
                                            if(element.count>=element.min){
                                                _dropDownData[i].count = _dropDownData[i].count-1; 
                                                setDropDownData(_dropDownData)
                                            }
                                            runIncrementDecrementChecker(_dropDownData, i, element)
                                            
                                        }} disabled={_.get(ButtonStates, `${element.code}.decrement`)}>-</Button>
                                        <div>
                                            <Typography className={classes.drop_title}>{element.count} {element.name}</Typography>
                                            <Typography className={classes.drop_sub}>{element.content}</Typography>
                                        </div>
                                        <Button onClick={o => {
                                            let _dropDownData = _.cloneDeep(dropDownData)
                                            let totalCount = _.sum(_.map(_dropDownData, o => o.count))
                                            if(totalCount<totalLimit){
                                                _dropDownData[i].count = _dropDownData[i].count+1; 
                                                totalCount += 1
                                                setPassengerCountLabel(
                                                    totalCount +  (totalCount > 0 ? " Passengers" : " Passenger" )
                                                )
                                                setDropDownData(_dropDownData)
                                            runIncrementDecrementChecker(_dropDownData, i, element)

                                            }else{

                                                let buttonState = _.cloneDeep(ButtonStates)
                                                _.forEach(_.keys(ButtonStates), o => {
                                                    _.set(buttonState, `${o}.increment`, true)
                                                    _.set(buttonState, `${o}.decrement`, true)
                                                })

                                            }

                                        }} disabled={_.get(ButtonStates, `${element.code}.increment`)}>+</Button>
                                        
                            </Grid>
                        </Grid>
                    )
                })}
        </Popover>
    </Grid>
   
  );

}

