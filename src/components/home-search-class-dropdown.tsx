import { FormControl, InputLabel, makeStyles, MenuItem, Select } from '@material-ui/core';
import React from 'react';
import 'react-google-flight-datepicker/dist/main.css';

const useStyles = makeStyles((theme) => ({
    search_btn: {
        backgroundColor: '#c60c30',
        color: '#fff',
        height: 55,
        width: '100%',
        textAlign: 'center'
      },
      formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        width: "100%"
      },
      selectEmpty: {
        marginTop: theme.spacing(2),
      },
    
}));


export default function HomeClassPicker() {

  const classes = useStyles();
  const [classValue, setClassValue] = React.useState<any | null>();
  

  return (


    <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel id="class_type_dropdown">Class</InputLabel>
        <Select

            labelId="class_type_dropdown"
            id="demo-simple-select-outlined"
            value={classValue}
            onChange={ newValue => setClassValue(newValue)}
            label="Class">
            <MenuItem value="">
            <em>None</em>
            </MenuItem>
            <MenuItem value="economy_class">Economy class</MenuItem>
            <MenuItem value="business_class">Business class</MenuItem>
            <MenuItem value="first_class">First class</MenuItem>
        </Select>
      </FormControl>



    
  );

}


