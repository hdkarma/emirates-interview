import { Button, Container, Grid, makeStyles, MenuItem, Select, Typography } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import React from 'react';
import { RangeDatePicker } from 'react-google-flight-datepicker';
import 'react-google-flight-datepicker/dist/main.css';
import emirates_logo from '../../src/emirates-logo.svg';
import { api_data } from '../api_data';
import HomePersonspicker from '../components/passenger-dropdown';
import HomeClassPicker from '../components/home-search-class-dropdown';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  option: {
    fontSize: 10,
  },
  option_title:{
    fontSize: 12,
  },
  option_code: {
    fontSize: 12,
    fontWeight: 'bold',
    backgroundColor: "#c9ebec",
    float: 'right',
    marginLeft: 15,
    padding: 6,
    borderRadius: 2,
  },
  emirates_logo: {
    width: 25,
    height: 25,
    marginLeft: 20,
  },
  full_width: {
    width: "100%"
  },
  group_label: {
    fontSize: 10,
    fontWeight: 100
  },
  search_btn: {
    backgroundColor: '#c60c30',
    color: '#fff',
    height: 55,
    width: '100%',
    textAlign: 'center'
  }
}));


export default function HomeDropdown() {
  const [arrival, setArrival] = React.useState<any | null>(null);
  const [arrivalValue, setArrivalValue] = React.useState('');


  const [departure, setDeparture] = React.useState<any | null>(null);
  const [departureValue, setDepartureValue] = React.useState('');

  const [date, setDateChange] = React.useState({startDate: new Date(), endDate: new Date()});


  const classes = useStyles();

  const filterOptions = createFilterOptions({
    matchFrom: 'any',
    stringify: (option:any) => option?.label + option?.airport + option?.state + option?.country
  });
  

  return (
    <div>

  <div className={classes.root}>
    <Container maxWidth="lg">
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <Autocomplete
            value={departure}
            onChange={(e, newValue) => {
              setDeparture(newValue);
            }}
            inputValue={departureValue}
            onInputChange={(e, newInputValue) => {
              setDepartureValue(newInputValue);
            }}
            id="departure"
            options={api_data}
            getOptionLabel={(params) => `${params.state} (${params.code})`}
            renderInput={(params) => <TextField {...params} label="Departure airport" variant="outlined" />}
            renderOption={(params) => <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              className={classes.full_width}
            >
              <div> 
                <Typography className={classes.option_title}><b>{params.state}</b>{params.country}</Typography>
                <Typography className={classes.option}>{params.airport}</Typography>
              </div>
              <div>
                <img src={emirates_logo} className={classes.emirates_logo} alt="logo_departure"/>
                <Typography align="center" className={classes.option_code}>{params.code}</Typography>
              </div>
            </Grid> }
          />
        </Grid>
        <Grid item xs={4}>
          <Autocomplete
            value={arrival}
            onChange={(e, arrival) => {
              setArrival(arrival);
            }}
            inputValue={arrivalValue}
            onInputChange={(event, newInputValue) => {
              setArrivalValue(newInputValue);
            }}
            id="arrival"
            // options={api_data}
            getOptionLabel={(params) => `${params.state} (${params.code})`}
            renderInput={(params) => <TextField {...params} label="Arrival airport" variant="outlined" />}
            filterOptions={filterOptions} 
            options={api_data.sort((a, b) => b.category?.localeCompare(a?.category))}
            // renderGroup={(params)=> <div>
            //   <Typography {...params} className={classes.group_label}>{params}</Typography>
            // </div>}
            groupBy={(option) => option.category}
            renderOption={(params) => 
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                className={classes.full_width}
              >
                <div> 
                  <Typography className={classes.option_title}><b>{params.state}</b>{params.country}</Typography>
                  <Typography className={classes.option}>{params.airport}</Typography>
                </div>
                <div>
                  <img src={emirates_logo} className={classes.emirates_logo} alt="logo_arrival"/>
                  <Typography align="center" className={classes.option_code}>{params.code}</Typography>
                </div>
              </Grid>
             }
          />
        </Grid>
        <Grid>
          <RangeDatePicker
            startDate={date?.startDate}
            endDate={date?.endDate}
            onChange={(startDate: any, endDate: any) => setDateChange({startDate: startDate, endDate: endDate})}
            minDate={new Date(1900, 0, 1)}
            maxDate={new Date(2100, 0, 1)}
            dateFormat="DD MMM YYYY"
            monthFormat="MMM YYYY"
            startDatePlaceholder="Start Date"
            endDatePlaceholder="End Date"
            disabled={false}
            className="my-own-class-name"
            startWeekDay="monday"
          />
        </Grid>
        <Grid item xs={4}>
          <HomePersonspicker/>
        </Grid>

        <Grid item xs={4}>
          <HomeClassPicker/>
        </Grid>
        <Grid item xs={4}>
          <Button className={classes.search_btn}>Search Flights</Button>
        </Grid>

        
      </Grid>
      </Container>

      

    </div>

      <br />
      
    </div>
  );

}

