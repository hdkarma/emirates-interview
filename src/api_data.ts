export const api_data = [{    
    label:"Abidjan, Côte d'Ivoire (Ivory Coast) Felix Houphouet Boigny Airport ABJ Operated by Emirates",
    state : "Abidjan,", 
    country: "Côte d'Ivoire(Ivory Coast)",
    airport: "Felix Houphouet Boigny Airport",
    code: "ABJ",
    category: "All locations"
  
  },
  {
    label:"Abuja, Nigeria Abuja Nnamdi Azikiwe International Airport ABV Operated by Emirates",
    state : "Abuja,", 
    country: "Nigeria",
    airport: "Abuja Nnamdi Azikiwe International Airport",
    code: "ABV",
    category: "All locations"
  },
  
  {
    label:"Accra, Ghana Kotoka International Airport ACC Operated by Emirates",
    state : "Accra,", 
    country: "Ghana",
    airport: "Kotoka International Airport",
    code: "ACC",
    category: "All locations"
  },
  
  {
    label:"Addis Ababa, Ethiopia Bole International Airport ADD Operated by Emirates",
    state : "Addis Ababa,", 
    country: "Ethiopia",
    airport: "Bole International Airport",
    code: "ADD",
    category: "All locations"
  },
  
  {
    label:"Adelaide, Australia Adelaide Airport ADL Operated by Emirates",
    state : "Adelaide,", 
    country: "Australia",
    airport: "Adelaide Airport",
    code: "ADL",
    category: "All locations"
  },
  
  {
    label:"Auckland, New Zealand Auckland International Airport AKL Operated by Emirates",
    state : "Auckland,", 
    country: "New Zealand",
    airport: "Auckland International Airport",
    code: "AKL",
    category: "All locations"
  },
  
  {
    label:"Algiers, Algeria Houari Boumediene Airport ALG Operated by Emirates",
    state : "Algiers,", 
    country: "Algeria",
    airport: "Houari Boumediene Airport",
    code: "ALG",
    category: "All locations"
  },
  
  {
    label:"Ahmedabad, India Sardar Vallabhbhai Patel International Airport AMD Operated by Emirates",
    state : "Ahmedabad,", 
    country: "India",
    airport: "Sardar Vallabhbhai Patel International Airport",
    code: "AMD",
    category: "All locations"
  },
  
  {
    label:"Amman, Jordan Queen Alia International Airport AMM Operated by Emirates",
    state : "Amman,", 
    country: "Jordan",
    airport: "Queen Alia International Airport",
    code: "AMM",
    category: "All locations"
  },
  
  {
    label:"Amsterdam, Netherlands Amsterdam Airport Schiphol AMS Operated by Emirates",
    state : "Amsterdam,", 
    country: "Netherlands",
    airport: "Amsterdam Airport Schiphol",
    code: "AMS",
    category: "All locations"
  },
  
  {
    label:"Stockholm, Sweden Stockholm-Arlanda Airport ARN Operated by Emirates",
    state : "Stockholm,", 
    country: "Sweden",
    airport: "Stockholm-Arlanda Airport",
    code: "ARN",
    category: "All locations"
  },
  
  {
    label:"Athens, Greece Eleftherios Venizelos Airport ATH Operated by Emirates",
    state : "Athens,", 
    country: "Greece",
    airport: "Eleftherios Venizelos Airport",
    code: "ATH",
    category: "All locations"
  },
  
  {
    label:"Bahrain, Bahrain Bahrain International Airport BAH Operated by Emirates",
    state : "Bahrain,", 
    country: "Bahrain",
    airport: "Bahrain International Airport",
    code: "BAH",
    category: "All locations"
  },
  
  {
    label:"Barcelona, Spain El Prat Airport BCN Operated by Emirates",
    state : "Barcelona,", 
    country: "Spain",
    airport: "El Prat Airport",
    code: "BCN",
    category: "All locations"
  },
  
  {
    label:"Beirut, Lebanon Rafic Hariri International Airport BEY Operated by Emirates",
    state : "Beirut,", 
    country: "Lebanon",
    airport: "Rafic Hariri International Airport",
    code: "BEY",
    category: "All locations"
  },
  
  {
    label:"Baghdad, Iraq Baghdad International Airport BGW Operated by Emirates",
    state : "Baghdad,", 
    country: "Iraq",
    airport: "Baghdad International Airport",
    code: "BGW",
    category: "All locations"
  },
  
  {
    label:"Birmingham, United Kingdom Birmingham International Airport BHX Operated by Emirates",
    state : "Birmingham,", 
    country: "United Kingdom", airport: "Birmingham International Airport",
    code: "BHX",
    category: "All locations"
  },           
  
  
  {
    label:"Bangkok, Thailand Suvarnabhumi International Airport BKK Operated by Emirates",
    state : "Bangkok,", 
    country: "Thailand",
    airport: "Suvarnabhumi International Airport",
    code: "BKK",
    category: "All locations"
  },
  
  {
    label:"Bologna, Italy Bologna Airport BLQ Operated by Emirates",
    state : "Bologna,", 
    country: "Italy",
    airport: "Bologna Airport",
    code: "BLQ",
    category: "All locations"
  },
  
  {
    label:"Bengaluru (Bangalore), India Bengaluru International Airport BLR Operated by Emirates",
    state : "Bengaluru, Bangalore),", 
    country: "India",
    airport: "Bengaluru International Airport",
    code: "BLR",
    category: "All locations"
  },           
  
  
  {
    label:"Brisbane, Australia Brisbane Airport BNE Operated by Emirates",
    state : "Brisbane,", 
    country: "Australia",
    airport: "Brisbane Airport",
    code: "BNE",
    category: "All locations"
  },
  
  {
    label:"Mumbai (Bombay), India Chhatrapati Shivaji International BOM Operated by Emirates",
    state : "Mumbai (Bombay),", 
    country:"India",
    airport: "Chhatrapati Shivaji International",
    code: "BOM",
    category: "All locations"
  },           
  
  
  {
    label:"Boston, United States Logan International Airport BOS Operated by Emirates",
    state : "Boston,", 
    country: "United States",
    airport: "Logan International Airport",
    code: "BOS",
    category: "All locations"
  },
  
  {
    label:"Brussels, Belgium Brussels Airport BRU Operated by Emirates",
    state : "Brussels,", 
    country: "Belgium",
    airport: "Brussels Airport",
    code: "BRU",
    category: "All locations"
  },
  
  {
    label:"Basra, Iraq Basra International Airport BSR Operated by Emirates",
    state : "Basra,", 
    country: "Iraq",
    airport: "Basra International Airport",
    code: "BSR",
    category: "All locations"
  },
  
  {
    label:"Budapest, Hungary Ferenc Liszt International Airport BUD Operated by Emirates",
    state : "Budapest,", 
    country: "Hungary",
    airport: "Ferenc Liszt International Airport",
    code: "BUD",
    category: "All locations"
  },
  
  {
    label:"Cairo, Egypt Cairo International Airport CAI Operated by Emirates",
    state : "Cairo,", 
    country: "Egypt",
    airport: "Cairo International Airport",
    code: "CAI",
    category: "All locations"
  },
  
  {
    label:"Guangzhou, China Guangzhou Baiyun International Airport CAN Operated by Emirates",
    state : "Guangzhou,", 
    country: "China",
    airport: "Guangzhou Baiyun International Airport",
    code: "CAN",
    category: "All locations"
  },
  
  {
    label:"Kolkata, India Netaji Subhas Chandra Bose International Airport CCU Operated by Emirates",
    state : "Kolkata,", 
    country: "India",
    airport: "Netaji Subhas Chandra Bose International Airport",
    code: "CCU",
    category: "All locations"
  },
  
  {
    label:"Paris, France Charles De Gaulle Airport CDG Operated by Emirates",
    state : "Paris,", 
    country: "France",
    airport: "Charles De Gaulle Airport",
    code: "CDG",
    category: "All locations"
  },
  
  {
    label:"Cebu, Philippines Mactan-Cebu International Airport CEB Operated by Emirates",
    state : "Cebu,", 
    country: "Philippines",
    airport: "Mactan-Cebu International Airport",
    code: "CEB",
    category: "All locations"
  },
  
  {
    label:"Jakarta, Indonesia Soekarno-Hatta International Airport CGK Operated by Emirates",
    state : "Jakarta,", 
    country: "Indonesia",
    airport: "Soekarno-Hatta International Airport",
    code: "CGK",
    category: "All locations"
  },
  
  {
    label:"Christchurch, New Zealand Christchurch International Airport CHC Operated by Emirates",
    state : "Christchurch,", 
    country: "New Zealand",
    airport: "Christchurch International Airport",
    code: "CHC",
    category: "All locations"
  },           
  
  
  {
    label:"Conakry, Guinea Conakry International Airport CKY Operated by Emirates",
    state : "Conakry,", 
    country: "Guinea",
    airport: "Conakry International Airport",
    code: "CKY",
    category: "All locations"
  },
  
  {
    label:"Colombo, Sri Lanka Bandaranaike Airport CMB Operated by Emirates",
    state : "Colombo,", 
    country: "Sri Lanka",
    airport: "Bandaranaike Airport",
    code: "CMB",
    category: "All locations"
  },
  
  {
    label:"Casablanca, Morocco Mohammed V International Airport CMN Operated by Emirates",
    state : "Casablanca,", 
    country: "Morocco",
    airport: "Mohammed V International Airport",
    code: "CMN",
    category: "All locations"
  },
  
  {
    label:"Kochi (Cochin), India Kochi International Airport COK Operated by Emirates",
    state : "Kochi (Cochin),", 
    country: "India",
    airport: "Kochi International Airport",
    code: "COK",
    category: "All locations"
  },           
  
  
  {
    label:"Copenhagen, Denmark Copenhagen Airport CPH Operated by Emirates",
    state : "Copenhagen,", 
    country: "Denmark",
    airport: "Copenhagen Airport",
    code: "CPH",
    category: "All locations"
  },
  
  {
    label:"Cape Town, South Africa Cape Town International Airport CPT Operated by Emirates",
    state : "Cape Town,", 
    country: "South Africa",
    airport: "Cape Town International Airport",
    code: "CPT",
    category: "All locations"
  },
  
  {
    label:"Clark, Philippines Clark International Airport, Manila Angeles City CRK Operated by Emirates",
    state : "Clark,", 
    country: "Philippines",
    airport: "Clark International Airport, Manila Angeles City",
    code: "CRK",
    category: "All locations"
  },
  
  {
    label:"Dhaka, Bangladesh Hazrat Shahjalal International Airport DAC Operated by Emirates",
    state : "Dhaka,", 
    country: "Bangladesh",
    airport: "Hazrat Shahjalal International Airport",
    code: "DAC",
    category: "All locations"
  },
  
  {
    label:"Dar Es Salaam, Tanzania Dar Es Salaam International Airport DAR Operated by Emirates",
    state : "Dar Es Salaam,", 
    country:"Tanzania",
    airport: "Dar Es Salaam International Airport",
    code: "DAR",
    category: "All locations"
  },           
  
  
  {
    label:"New Delhi, India Indira Gandhi International Airport DEL Operated by Emirates",
    state : "New Delhi,", 
    country: "India",
    airport: "Indira Gandhi International Airport",
    code: "DEL",
    category: "All locations"
  },
  
  {
    label:"Dallas, United States Dallas/Fort Worth International Airport DFW Operated by Emirates",
    state : "Dallas,", 
    country: "United States",
    airport: "Dallas/Fort Worth International Airport",
    code: "DFW",
    category: "All locations"
  },
  
  {
    label:"Moscow, Russia Domodedovo Airport DME Operated by Emirates",
    state : "Moscow,", 
    country: "Russia",
    airport: "Domodedovo Airport",
    code: "DME",
    category: "All locations"
  },
  
  {
    label:"Dammam, Saudi Arabia King Fahd International Airport DMM Operated by Emirates",
    state : "Dammam,", 
    country: "Saudi Arabia",
    airport: "King Fahd International Airport",
    code: "DMM",
    category: "All locations"
  },
  
  {
    label:"Bali, Indonesia Ngurah Rai International Airport DPS Operated by Emirates",
    state : "Bali,", 
    country: "Indonesia",
    airport: "Ngurah Rai International Airport",
    code: "DPS",
    category: "All locations"
  },
  
  {
    label:"Dakar, Senegal Dakar Blaise Diagne International Airport DSS Operated by Emirates",
    state : "Dakar,", 
    country: "Senegal",
    airport: "Dakar Blaise Diagne International Airport",
    code: "DSS",
    category: "All locations"
  },
  
  {
    label:"Dublin, Ireland Dublin Airport DUB Operated by Emirates",
    state : "Dublin,", 
    country: "Ireland",
    airport: "Dublin Airport",
    code: "DUB",
    category: "All locations"
  },
  
  {
    label:"Durban, South Africa King Shaka International Airport DUR Operated by Emirates",
    state : "Durban,", 
    country: "South Africa",
    airport: "King Shaka International Airport",
    code: "DUR",
    category: "All locations"
  },
  
  {
    label:"Düsseldorf, Germany Düsseldorf Airport DUS Operated by Emirates",
    state : "Düsseldorf,", 
    country: "Germany",
    airport: "Düsseldorf Airport",
    code: "DUS",
    category: "All locations"
  },
  
  {
    label:"Dubai, United Arab Emirates Dubai International Airport DXB Operated by Emirates",
    state : "Dubai,", 
    country: "United Arab Emirates",
    airport: "Dubai International Airport",
    code: "DXB",
    category: "My location"
  },
  
  {
    label:"Entebbe, Uganda Entebbe Airport EBB Operated by Emirates",
    state : "Entebbe,", 
    country: "Uganda",
    airport: "Entebbe Airport",
    code: "EBB",
    category: "All locations"
  },
  
  {
    label:"Edinburgh, United Kingdom Edinburgh Airport EDI Operated by Emirates",
    state : "Edinburgh,", 
    country: "United Kingdom",
    airport: "Edinburgh Airport",
    code: "EDI",
    category: "All locations"
  },           
  
  
  {
    label:"New York, United States Newark Liberty International Airport EWR Operated by Emirates",
    state : "New York,", 
    country: "United States",
    airport: "Newark Liberty International Airport",
    code: "EWR",
    category: "All locations"
  },
  
  {
    label:"Buenos Aires, Argentina Ministro Pistarini International Airport EZE Operated by Emirates",
    state : "Buenos Aires,", 
    country:"Argentina",
    airport: "Ministro Pistarini International Airport",
    code: "EZE",
    category: "All locations"
  },           
  
  
  {
    label:"Rome, Italy Fiumicino Airport FCO Operated by Emirates",
    state : "Rome,", 
    country: "Italy",
    airport: "Fiumicino Airport",
    code: "FCO",
    category: "All locations"
  },
  
  {
    label:"Frankfurt am Main, Germany Rhein-Main International Airport FRA Operated by Emirates",
    state : "Frankfurt am Main,",
    country: "Germany",
    airport: "Rhein-Main International Airport",
    code: "FRA",
    category: "All locations"
  },           
  
  
  {
    label:"Rio de Janeiro, Brazil Antônio Carlos Jobim International Airport (Galeão) GIG Operated by Emirates",
    state : "Rio de Janeiro,", 
    country:"Brazil",
    airport: "Antônio Carlos Jobim International Airport (Galeão)",
    code: "GIG",
    category: "All locations"
  },           
  
  
  {
    label:"Glasgow, United Kingdom Glasgow International Airport GLA Operated by Emirates",
    state : "Glasgow,", 
    country: "United Kingdom",
    airport: "Glasgow International Airport",
    code: "GLA",
    category: "All locations"
  }]